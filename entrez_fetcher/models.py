""" Models for entrez_fetcher

This module contains the dataclasses used to represent the data returned by the
NCBI Entrez API.
"""

from __future__ import annotations
from dataclasses import fields
from enum import Enum

import re
from datetime import datetime
from typing import List, Optional
import polars as pl

from pydantic import validator
from pydantic.fields import ModelField
from pydantic.dataclasses import dataclass

_SNAKE_CASE_PATTERN = re.compile(r"(?<!^)(?=[A-Z])")


def pascal2snake(name: str) -> str:
    return _SNAKE_CASE_PATTERN.sub("_", name).lower()


TAXON_SCHEMA = {
    "tax_id": pl.Int64,
    "scientific_name": pl.Utf8,
    "rank": pl.Utf8,
    "division": pl.Utf8,
    "parent_tax_id": pl.Int64,
    "genetic_code": pl.Struct([pl.Field("id", pl.Int64), pl.Field("name", pl.Utf8)]),
    "mito_genetic_code": pl.Struct(
        [pl.Field("id", pl.Int64), pl.Field("name", pl.Utf8)]
    ),
    "lineage": pl.Utf8,
    "lineage_ex": pl.List(
        pl.Struct(
            [
                pl.Field("tax_id", pl.Int64),
                pl.Field("scientific_name", pl.Utf8),
                pl.Field("rank", pl.Utf8),
            ]
        )
    ),
    "create_date": pl.Utf8,
    "update_date": pl.Utf8,
    "pub_date": pl.Utf8,
}

GS_DOCSUM_SCHEMA = {
    "id": pl.Int64,
    "caption": pl.Utf8,
    "title": pl.Utf8,
    "extra": pl.Utf8,
    "gi": pl.Int64,
    "create_date": pl.Utf8,
    "update_date": pl.Utf8,
    "flags": pl.Int64,
    "tax_id": pl.Int64,
    "length": pl.Int64,
    "status": pl.Utf8,
    "replaced_by": pl.Utf8,
    "comment": pl.Utf8,
    "accession_version": pl.Utf8,
}

ASSEMBLY_DOCSUM_SCHEMA = {
    "#assembly_accession": pl.Utf8,
    "bioproject": pl.Utf8,
    "biosample": pl.Utf8,
    "wgs_master": pl.Utf8,
    "refseq_category": pl.Utf8,
    "taxid": pl.Int64,
    "species_taxid": pl.Int64,
    "organism_name": pl.Utf8,
    "infraspecific_name": pl.Utf8,
    "isolate": pl.Utf8,
    "version_status": pl.Utf8,
    "assembly_level": pl.Utf8,
    "release_type": pl.Utf8,
    "genome_rep": pl.Utf8,
    "seq_rel_date": pl.Utf8,
    "asm_name": pl.Utf8,
    "asm_submitter": pl.Utf8,
    "gbrs_paired_asm": pl.Utf8,
    "paired_asm_comp": pl.Utf8,
    "ftp_path": pl.Utf8,
    "excluded_from_refseq": pl.Utf8,
    "relation_to_type_material": pl.Utf8,
    "asm_not_live_date": pl.Utf8,
    "assembly_type": pl.Utf8,
    "group": pl.Utf8,
    "genome_size": pl.Int64,
    "genome_size_ungapped": pl.Int64,
    "gc_percent": pl.Float64,
    "replicon_count": pl.Int64,
    "scaffold_count": pl.Int64,
    "contig_count": pl.Int64,
    "annotation_provider": pl.Utf8,
    "annotation_name": pl.Utf8,
    "annotation_date": pl.Utf8,
    "total_gene_count": pl.Int64,
    "protein_coding_gene_count": pl.Int64,
    "non_coding_gene_count": pl.Int64,
    "pubmed_id": pl.Utf8,
    "accession": pl.Utf8,
    "version": pl.Int64,
}


class DB(Enum):
    bioproject = "bioproject"
    biosample = "biosample"
    biosystems = "biosystems"
    books = "books"
    cdd = "cdd"
    gap = "gap"
    dbvar = "dbvar"
    epigenomics = "epigenomics"
    nucest = "nucest"
    gene = "gene"
    genome = "genome"
    gds = "gds"
    geoprofiles = "geoprofiles"
    nucgss = "nucgss"
    homologene = "homologene"
    mesh = "mesh"
    toolkit = "toolkit"
    ncbisearch = "ncbisearch"
    nlmcatalog = "nlmcatalog"
    nuccore = "nuccore"
    omia = "omia"
    popset = "popset"
    probe = "probe"
    protein = "protein"
    proteinclusters = "proteinclusters"
    pcassay = "pcassay"
    pccompound = "pccompound"
    pcsubstance = "pcsubstance"
    pubmed = "pubmed"
    pmc = "pmc"
    snp = "snp"
    sra = "sra"
    structure = "structure"
    taxonomy = "taxonomy"
    unigene = "unigene"
    unists = "unists"


@dataclass
class Docsum:
    id: Optional[int] = None
    caption: Optional[str] = None
    title: Optional[str] = None
    extra: Optional[str] = None
    gi: Optional[int] = None
    create_date: Optional[str] = None
    update_date: Optional[str] = None
    flags: Optional[int] = None
    tax_id: int = 0
    length: Optional[int] = None
    status: Optional[str] = None
    replaced_by: Optional[str] = None
    comment: Optional[str] = None
    accession_version: Optional[str] = None

    @classmethod
    def get_fields_name(cls) -> List[str]:
        return [field.name for field in fields(cls)]

    @validator("create_date", "update_date", pre=True)
    def _parse_date(cls, v):
        if v is None:
            return v
        try:
            return datetime.strptime(v, "%Y/%m/%d").date().isoformat()
        except ValueError:
            pass
        return v

    @validator("*")
    def _force_stdtype(cls, v, field: ModelField):
        if v is None:
            return v
        if field.type_ in [str, int, dict]:
            return field.type_(v)
        return v

    @classmethod
    def from_dict(cls, d):
        return cls(
            **{
                fkey: value
                for key, value in d.items()
                if (fkey := pascal2snake(key)) in cls.get_fields_name()
            }
        )


@dataclass
class GeneticCode:
    id: int
    name: str

    @classmethod
    def get_fields_name(cls) -> List[str]:
        return [field.name for field in fields(cls)]

    @validator("*")
    def _force_stdtype(cls, v, field: ModelField):
        if field.type_ in [str, int, dict]:
            return field.type_(v)
        return v


@dataclass
class LineageEx:
    tax_id: int
    scientific_name: str
    rank: str

    @classmethod
    def get_fields_name(cls) -> List[str]:
        return [field.name for field in fields(cls)]

    @validator("*")
    def _force_stdtype(cls, v, field: ModelField):
        if field.type_ in [str, int, dict]:
            return field.type_(v)
        return v

    @classmethod
    def from_dict(cls, d):
        return cls(
            **{
                fkey: value
                for key, value in d.items()
                if (fkey := pascal2snake(key)) in cls.get_fields_name()
            }
        )


@dataclass
class Taxonomy:
    tax_id: int
    scientific_name: Optional[str] = None
    rank: Optional[str] = None
    division: Optional[str] = None
    parent_tax_id: Optional[int] = None
    genetic_code: Optional[GeneticCode] = None
    mito_genetic_code: Optional[GeneticCode] = None
    lineage: Optional[List[str]] = None
    lineage_ex: Optional[List[LineageEx]] = None
    create_date: Optional[str] = None
    update_date: Optional[str] = None
    pub_date: Optional[str] = None

    @classmethod
    def get_fields_name(cls) -> List[str]:
        return [field.name for field in fields(cls)]

    @classmethod
    def from_dict(cls, d):
        return cls(
            **{
                fkey: value
                for key, value in d.items()
                if (fkey := pascal2snake(key)) in cls.get_fields_name()
            }
        )

    @validator("*")
    def _force_stdtype(cls, v, field: ModelField):
        if v is None:
            return v
        if field.type_ in [str, int, dict]:
            return field.type_(v)
        return v

    @validator("genetic_code", pre=True)
    def _parse_gcode(cls, v):
        if v is None:
            return v
        return GeneticCode(id=v["GCId"], name=v["GCName"])

    @validator("mito_genetic_code", pre=True)
    def _parse_mgcode(cls, v):
        if v is None:
            return v
        return GeneticCode(id=v["MGCId"], name=v["MGCName"])

    @validator("create_date", "update_date", "pub_date", pre=True)
    def _parse_date(cls, v):
        if v is None:
            return v
        try:
            return datetime.strptime(v, "%Y/%m/%d %H:%M:%S").isoformat()
        except ValueError:
            pass
        return v

    @validator("lineage", pre=True)
    def _parse_lineage(cls, v):
        if v is None:
            return v
        return v.split("; ")

    @validator("lineage_ex", pre=True)
    def _parse_lineage_ex(cls, v):
        if v is None:
            return v
        return list(map(LineageEx.from_dict, v))
