.. Entrez Fetcher documentation master file, created by
   sphinx-quickstart on Mon Sep  4 09:55:31 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Entrez Fetcher's documentation!
==========================================

Fetch data from the entrez e-utilities. Provide clever batching, client-side
rate limit (and automatic retry if even the rate limitation fail, as the entrez
api can be more conservative than what is announced).

You can provide your email and api-key to leverage a bit this limitation (8 request per
second instead of 3 if an api-key is provided).

The requests are batched to send as much as parallel request as allowed. Moreover,
fetching multiple uids at once will always be more efficient than one at a time.

This fetcher is not supposed to give access to low-level e-utilities (esearch, efetch,
esummary...), but to give access to high-level routines to automatically get entry info,
download sequences or get taxonomy data.

.. warning::

    for now, this library is not ready for production usage. The API could change
    and there is no unit-testing. That being said, the library is really simple and is only
    dedicated to one task : get data from the NCBI database.

.. note::

    This library use `polars <https://www.pola.rs/>`_ to store and manipulate
    data, and not the well-known pandas library.

    If you need to convert a polars DataFrame to a pandas DataFrame, you can
    simply use `polars.DataFrame.to_pandas()`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./quickstart
   ./api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
